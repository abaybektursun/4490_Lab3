package lab3in;

import ocsf.client.AbstractClient;

public class ChatClient extends AbstractClient{
    
    public ChatClient()
    {
        super("localhost",8300);
    }
    
    public ChatClient(String host, int port)
    {
        super(host, port);
    }
    
    public void handleMessageFromServer(Object arg0)
    {
        System.out.println("Server message sent to client " + (String)arg0 );
    }
    
    public void connectionException (Throwable exception) 
    {
        System.out.println("Connection Exception Occurred");
        System.out.println(exception.getMessage());
        exception.printStackTrace();
        
    }
    public void connectionEstablished() 
    {
        System.out.println("Client Connected");
    }
}
