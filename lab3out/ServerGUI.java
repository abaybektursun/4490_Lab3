package lab3out;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.io.IOException;
import javax.swing.UIManager;


public class ServerGUI extends JFrame
{
    private JLabel status;	
    private JButton listen;	
    private JButton close;	
    private JButton stop;	
    private JButton quit;	
    private String[] labels = {"Port", "Timeout"};
    private JTextField[] textFields = new JTextField[labels.length];
    private JTextArea log;
    
    private ChatServer server;
    
    private class EventHandler implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();  
            if(command.equals("Quit"))
            {System.exit(0);}
            //else{System.out.println(command + " Button Pressed");}
            
            if(command.equals("Listen"))
            {
                if(textFields[0].getText().trim().equals("") || textFields[1].getText().trim().equals(""))
                {
                    log.append("Port Number/timeout not entered before pressing Listen\n");
                }
                else
                {
                    server.setPort(Integer.parseInt(textFields[0].getText()));
                    server.setTimeout( Integer.parseInt(textFields[1].getText()) );
                    try {
                        server.listen();
                    } catch (IOException listen_exc) {
                        listen_exc.printStackTrace();
                    }
                }
                
            }
            
            if(command.equals("Stop"))
            {
                if(!server.isListening())
                {
                    log.append("Server not currently started\n");
                }
                else{
                    server.stopListening();
                }
            }
            
            if(command.equals("Close"))
            {
                if(!server.isListening())
                {
                    log.append("Server not currently started\n");
                }
                else
                {
                    try{
                        server.close();
                    }   
                    catch (IOException close_exc) {
                        close_exc.printStackTrace();
                    }
                }
               
            }
        }
    }
    
    public ServerGUI(String title)
    {
            int i = 0;
            this.setTitle(title);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            // North Panel
            JPanel north = new JPanel(new FlowLayout());     
            this.getContentPane().add(north, BorderLayout.NORTH);
            
            // Red connection text
            status = new JLabel("Not Connected");
            status.setForeground(Color.red);
            north.add(status);
            
            //South Panel
            JPanel south = new JPanel(new FlowLayout());
            this.getContentPane().add(south, BorderLayout.SOUTH);
            
            // 4 Buttons
            listen  = new JButton("Listen");
            close   = new JButton("Close");
            stop    = new JButton("Stop");
            quit    = new JButton("Quit");
            // Event Handling 
            listen.addActionListener(new EventHandler ()); 
            close.addActionListener (new EventHandler ()); 
            stop.addActionListener  (new EventHandler ()); 
            quit.addActionListener  (new EventHandler ()); 
            south.add(listen);
            south.add(close);
            south.add(stop);
            south.add(quit);
            
            // Center Panel
            JPanel center = new JPanel();
            center.setLayout( new BoxLayout(center, BoxLayout.Y_AXIS) );
            this.getContentPane().add(center, BorderLayout.CENTER);
            
            
            // Text fields ------------------------------------------//
            // Panels inside the central panel for text fields       //
            JPanel panel1 = new JPanel(new FlowLayout());            //
            JPanel panel2 = new JPanel(new FlowLayout());            //
                                                                     //
            JLabel panel1_lbl = new JLabel(labels[0] + "      ");     //
            panel1_lbl.setHorizontalAlignment(SwingConstants.LEFT);  //
            panel1.add( panel1_lbl );                                //
            textFields[0] = new JTextField(15);                      //
            textFields[0].setHorizontalAlignment(JTextField.LEADING);//
            panel1.add( textFields[0] );                             //
                                                                     //
            JLabel panel2_lbl = new JLabel(labels[1]);               //
            panel2_lbl.setHorizontalAlignment(SwingConstants.LEFT);  //
            panel2.add( panel2_lbl );                                //
            textFields[1] = new JTextField(15);                      //
            textFields[1].setHorizontalAlignment(JTextField.LEADING);//
            panel2.add( textFields[1] );                             //
                                                                     //
            center.add(panel1);                                      //
            center.add(panel2);                                      //
            //-------------------------------------------------------//
            
            // Log Area
            center.add( new JLabel("Server Log") );
            log = new JTextArea(5,20);
            log.setEditable(false);
            JScrollPane sa_scrollP = new JScrollPane(log);
            center.add(sa_scrollP);
            
            // Sides
            JPanel west = new JPanel(new FlowLayout());
            west.add(Box.createRigidArea(new Dimension(5,0)));
            this.getContentPane().add(west, BorderLayout.WEST);
            JPanel east = new JPanel(new FlowLayout());
            east.add(Box.createRigidArea(new Dimension(5,0)));
            this.getContentPane().add(east, BorderLayout.EAST);
            
            // No requirement for resizing
            this.setResizable(false);
            
            this.pack();
            this.setVisible(true);  
            
            server = new ChatServer();
            server.setLog(log);
            server.setStatus(status);
    }
    
    // Retrieves the corresponding JTextField based in input parameter index
    public JTextField getTextFieldAt(int index){return textFields[index];}
    
    // Returns the top JLabel of the GUI (initialized to "Not Connected")
    public JLabel getStatus(){return status;}
    
    // Returns the JTextArea pertaining to the data returned from the Server
    public JTextArea getLog(){return log;}

    public static void main(String[] args)
    {
        try {
            // Set cross-platform Java L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException e){}
        catch (ClassNotFoundException e) {}
        catch (InstantiationException e) {}
        catch (IllegalAccessException e) {}
        
        try{
            new ServerGUI(args[0]); //args[0] represents the title of the GUI
        }
        catch (ArrayIndexOutOfBoundsException aiooe){new ServerGUI("Client");}
    }
}