package lab3out;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.io.IOException;
import javax.swing.UIManager;


public class ClientGUI extends JFrame
{
    private JLabel status;	//From Lab 1 In
    private JButton connect;//From Lab 1 In
    private JButton submit;	//From Lab 1 In
    private JButton stop;	//From Lab 1 In
    private String[] labels = {"Client ID", "Server URL", "Server Port"};
    private JTextField[] textFields = new JTextField[labels.length];
    private JTextArea clientArea;
    private JTextArea serverArea;
    private ChatClient client;
    
    private class EventHandler implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();  
            if(command.equals("Submit"))
            {
                try {
                    client.sendToServer(clientArea.getText());
                } catch (IOException send_exc) {
                    send_exc.printStackTrace();
                }
                
            }
            else if (command.equals("Connect"))
            {
                client.setHost(textFields[1].getText().trim());
                client.setPort(Integer.parseInt(textFields[2].getText().trim()));
                try {
                    client.openConnection();
                } catch (IOException open_exc) {
                    open_exc.printStackTrace();
                }
            }
            else if (command.equals("Stop"))
            {
                try {
                    client.closeConnection();
                } catch (IOException close_exc) {
                    close_exc.printStackTrace();
                }
            }
        }
    }
    
    public ClientGUI(String title)
    {
            int i = 0;
            this.setTitle(title);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            // North Panel
            JPanel north = new JPanel(new FlowLayout());
            this.getContentPane().add(north, BorderLayout.NORTH);
            
            // Red connection text
            status = new JLabel("Not Connected");
            status.setForeground(Color.red);
            north.add(status);
            
            //South Panel
            JPanel south = new JPanel(new FlowLayout());
            this.getContentPane().add(south, BorderLayout.SOUTH);
            
            // 3 Buttons
            connect = new JButton("Connect");
            submit  = new JButton("Submit");
            stop    = new JButton("Stop");
            // Event Handling 
            connect.addActionListener(new EventHandler ()); 
            submit.addActionListener (new EventHandler ()); 
            stop.addActionListener   (new EventHandler ()); 
            south.add(connect);
            south.add(submit);
            south.add(stop);
            
            // Center Panel
            JPanel center = new JPanel();
            center.setLayout( new BoxLayout(center, BoxLayout.Y_AXIS) );
            this.getContentPane().add(center, BorderLayout.CENTER);
            
            
            // Text fields ------------------------------------------//
            // Panels inside the central panel for text fields       //
            JPanel panel1 = new JPanel(new FlowLayout());            //
            JPanel panel2 = new JPanel(new FlowLayout());            //
            JPanel panel3 = new JPanel(new FlowLayout());            //
                                                                     //
            JLabel panel1_lbl = new JLabel(labels[0] + "    ");      //
            panel1_lbl.setHorizontalAlignment(SwingConstants.LEFT);  //
            panel1.add( panel1_lbl );                                //
            textFields[0] = new JTextField(15);                      //
            textFields[0].setEditable(false);                        //
            textFields[0].setHorizontalAlignment(JTextField.LEADING);//
            panel1.add( textFields[0] );                             //
                                                                     //
            JLabel panel2_lbl = new JLabel(labels[1]);               //
            panel2_lbl.setHorizontalAlignment(SwingConstants.LEFT);  //
            panel2.add( panel2_lbl );                                //
            textFields[1] = new JTextField(15);                      //
            textFields[1].setHorizontalAlignment(JTextField.LEADING);//
            panel2.add( textFields[1] );                             //
                                                                     //
                                                                     //
            JLabel panel3_lbl = new JLabel(labels[2]);               //
            panel3_lbl.setHorizontalAlignment(SwingConstants.LEFT);  //
            panel3.add( panel3_lbl );                                //
            textFields[2] = new JTextField(15);                      //
            textFields[2].setHorizontalAlignment(JTextField.LEADING);//
            panel3.add( textFields[2] );                             //
                                                                     //
            center.add(panel1);                                      //
            center.add(panel2);                                      //
            center.add(panel3);                                      //
            //-------------------------------------------------------//
            
            // Client Area
            center.add( new JLabel("Enter Client Data Below") );
            clientArea = new JTextArea(5,20);
            JScrollPane ca_scrollP = new JScrollPane(clientArea);
            center.add(ca_scrollP);
            // Server Area
            center.add( new JLabel("Recieved Server Data") );
            serverArea = new JTextArea(5,20);
            serverArea.setEditable(false);
            JScrollPane sa_scrollP = new JScrollPane(serverArea);
            center.add(sa_scrollP);
            
            // Sides
            JPanel west = new JPanel(new FlowLayout());
            west.add(Box.createRigidArea(new Dimension(5,0)));
            this.getContentPane().add(west, BorderLayout.WEST);
            JPanel east = new JPanel(new FlowLayout());
            east.add(Box.createRigidArea(new Dimension(5,0)));
            this.getContentPane().add(east, BorderLayout.EAST);
            
            // No requirement for resizing
            this.setResizable(false);
            
            this.pack();
            this.setVisible(true);  
            
            // String[] labels = {"Client ID", "Server URL", "Server Port"};
            // JTextField[] textFields = new JTextField[labels.length];
            
            client = new ChatClient();
            client.setStatus(status);
            client.setServerMsg(serverArea);
            client.setClientID(textFields[0]);
    }
    
    // Retrieves the corresponding JTextField based in input parameter index
    public JTextField getTextFieldAt(int index){return textFields[index];}
    
    // Returns the top JLabel of the GUI (initialized to "Not Connected")
    public JLabel getStatus(){return status;}
    
    // Returns the JTextArea pertaining to the data returned from the Client
    public JTextArea getClientArea(){return clientArea;}
    
    // Returns the JTextArea pertaining to the data returned from the Server
    public JTextArea getServerArea(){return serverArea;}

    public static void main(String[] args)
    {
        try {
            // Set cross-platform Java L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException e){}
        catch (ClassNotFoundException e) {}
        catch (InstantiationException e) {}
        catch (IllegalAccessException e) {}
        
        try{
            new ClientGUI(args[0]); //args[0] represents the title of the GUI
        }
        catch (ArrayIndexOutOfBoundsException aiooe){new ClientGUI("Client");}
    }
}