package lab3out;

import ocsf.server.AbstractServer;
import ocsf.server.ConnectionToClient;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.io.IOException;
import javax.swing.UIManager;

public class ChatServer extends AbstractServer {
    
    private JTextArea log; //Corresponds to JTextArea of ServerGUI
    private JLabel status; //Corresponds to the JLabel of ServerGUI
    
	public ChatServer() {
		super(12345);
		super.setTimeout(500);
	}

	@Override
	protected void handleMessageFromClient(Object arg0, ConnectionToClient arg1) {
        this.log.append("Client " + arg1.getId() + " " + (String)arg0 + "\n");
        try{
            arg1.sendToClient((String)arg0);
        }   
        catch (IOException send_exc) {
            send_exc.printStackTrace();
        }
        

	}
	
	public void listeningException(Throwable exception) {
		this.log.append(exception.toString() + "\n");
        this.status.setForeground(Color.red);
        this.status.setText("Exception Occurred when Listening");
	}
	
	public void serverStarted() {
		this.log.append("Server Started\n");
        this.status.setForeground(Color.green);
        this.status.setText("Listening");
	}
    
    public void serverStopped()
    {
        this.log.append("Server Stopped Accepting New Clients - Press Listen to Start Accepting New Clients\n");
        this.status.setForeground(Color.red);
        this.status.setText("Stopped");
    }
    
    public void serverClosed()
    {
        this.log.append("Server and all current clients are closed - Press Listen to Restart\n");
        this.status.setForeground(Color.red);
        this.status.setText("Close");
    }
    
    public void setLog(JTextArea log)
    {
        this.log = log;
    }
    public void setStatus(JLabel status)
    {
        this.status = status;
    }
    
    public void clientConnected(ConnectionToClient client)
    {
         try{
            client.sendToClient("username:Client-" + client.getId());
        }   
        catch (IOException send_exc) {
            send_exc.printStackTrace();
        }
        
        this.log.append("Client " + client.getId() + " Connected" + "\n");
    }

}
